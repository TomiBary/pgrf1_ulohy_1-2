package rasterdata;

import org.jetbrains.annotations.NotNull;
import rasterizationops.CircleRasterizer;
import rasterizationops.LineRasterizer;

import java.awt.*;

public class CircleData<T> implements ObjectData<T> {

    private final double x1, y1, radius, angle1, angle2;
    private T color;
    private final CircleRasterizer<T> circleRasterizer;
    private final LineRasterizer<T> lineRasterizer;

    public CircleData(CircleRasterizer<T> circleRasterizer, LineRasterizer<T> lineRasterizer, double x1, double y1, double radius, double angle1, double angle2, T color) {
        this.x1 = x1;
        this.y1 = y1;
        this.radius = radius;
        this.angle1 = angle1;
        this.angle2 = angle2;
        this.color = color;
        this.lineRasterizer = lineRasterizer;
        this.circleRasterizer = circleRasterizer;
    }

    @NotNull
    @Override
    public RasterImage<T> drawElement(@NotNull RasterImage<T> rasterImage) {
        if (angle2 == 0)
            rasterImage = circleRasterizer.drawCircle(rasterImage,x1,y1,radius,color);
        else
            rasterImage = circleRasterizer.drawCircleSector(rasterImage, lineRasterizer, x1, y1, radius, angle1, angle2, color);
        return rasterImage;
    }
}
