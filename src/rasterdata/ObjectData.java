package rasterdata;

import org.jetbrains.annotations.NotNull;

public interface ObjectData<T> {

    @NotNull RasterImage<T> drawElement(
            @NotNull RasterImage<T> rasterImage);
}
