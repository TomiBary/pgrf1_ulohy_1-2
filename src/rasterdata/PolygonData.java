package rasterdata;

import org.jetbrains.annotations.NotNull;
import rasterizationops.LineRasterizer;
import transforms.Point2D;

import java.util.ArrayList;

public class PolygonData<T> implements ObjectData<T>{

    private ArrayList<Point2D> list = new ArrayList<Point2D>();
    private T color;
    private LineRasterizer<T> lineRasterizer;

    public PolygonData() {
        list = new ArrayList<>();
    }

    public PolygonData(LineRasterizer<T> lineRasterizer, ArrayList<Point2D> list, T color) {
        this.list = list;
        this.color = color;
        this.lineRasterizer = lineRasterizer;
    }

    public void addPoint(Point2D point) {
        list.add(point);
    }


    public Point2D getPoint(int index) {
        if(index < 0 || index > list.size()-1) {
            throw new IndexOutOfBoundsException();
        }
        return list.get(index);
    }

    public int getSize() {
        return list.size();
    }

    public void clearList(){
        list.clear();
    }

    @NotNull
    @Override
    public RasterImage<T> drawElement(@NotNull RasterImage<T> rasterImage) {
        for(int i=0; i < list.size(); i++) {
            Point2D from = list.get(i);
            Point2D to = list.get((i+1)%list.size());
            rasterImage = lineRasterizer.rasterize(rasterImage, from.getX(), from.getY(), to.getX(), to.getY(), color);
        }
        return rasterImage;
    }

    public void removeLastPoint() {
        if (list.size() > 0) {list.remove(list.size()-1);}
    }

    public ArrayList<Point2D> getList() {
        return list;
    }

    public T getColor() {
        return color;
    }

    public LineRasterizer<T> getLineRasterizer() {
        return lineRasterizer;
    }

    public void setColor(T color) {
        this.color = color;
    }

    public void setLineRasterizer(LineRasterizer<T> lineRasterizer) {
        this.lineRasterizer = lineRasterizer;
    }
}
