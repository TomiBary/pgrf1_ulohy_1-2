package rasterdata;

import org.jetbrains.annotations.NotNull;
import rasterizationops.LineRasterizer;

import java.awt.*;

public class LineData<T> implements ObjectData<T>{
    private final double fromX, fromY, toX, toY;
    private final LineRasterizer<T> lineRasterizer;
    private final T color;

    public LineData(LineRasterizer<T> lineRasterizer, double fromX, double fromY, double toX, double toY, T color) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.lineRasterizer = lineRasterizer;
        this.color = color;
    }

    public double getFromX() {
        return fromX;
    }

    public double getFromY() {
        return fromY;
    }

    public double getToX() {
        return toX;
    }

    public double getToY() {
        return toY;
    }

    @NotNull
    @Override
    public RasterImage<T> drawElement(@NotNull RasterImage<T> rasterImage) {
        rasterImage = lineRasterizer.rasterize(rasterImage, fromX, fromY, toX, toY, color);
        return rasterImage;
    }
}
