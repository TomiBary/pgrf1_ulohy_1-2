package solids;

import io.vavr.collection.IndexedSeq;
import io.vavr.collection.Seq;
import org.jetbrains.annotations.NotNull;
import transforms.Point3D;

public interface Solid<VertexType, TopoType> {
    class Part<TopologyType> {
        private final int startIndex, numberOfPrimitives;
        private final @NotNull TopologyType topology;
        public Part(int startIndex, int numberOfPrimitives, @NotNull TopologyType topology) {
            this.startIndex = startIndex;
            this.numberOfPrimitives = numberOfPrimitives;
            this.topology = topology;
        }
        public int getStartIndex() {
            return startIndex;
        }
        public int getNumberOfPrimitives() {
            return numberOfPrimitives;
        }
        public TopologyType getTopology() {
            return topology;
        }
    }
    @NotNull IndexedSeq<VertexType> getVertices();
    @NotNull IndexedSeq<Integer> getIndices();
    @NotNull Seq<Part<TopoType>> getPart();
}
