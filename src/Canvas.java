import org.jetbrains.annotations.NotNull;
import rasterdata.*;
import rasterizationops.*;
import transforms.Point2D;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

public class Canvas {

    private final JFrame frame;
    private final JPanel panel, panelMenu;
    private final BufferedImage img;
    private final @NotNull ImagePresenter<Color, Graphics> presenter;
    private final @NotNull LineRasterizer<Color> lineRasterizer;
    private final @NotNull CircleRasterizer<Color> circleRasterizer;
    private int drawModeIndex = 0;
    private DrawMode dm;
    private boolean firstStep = true, isntPolygonSaved;
    private double angle1, angle2, radius;
    private PolygonData<Color> polygonMemory;
    private JLabel modelbl;
    private ArrayList<ObjectData<Color>> arrRast = new ArrayList<>();
    private @NotNull RasterImage<Color> rasterImage;
    private int startC, startR;

    public Canvas(final int width, final int height) {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //What to draw mode
        dm = DrawMode.values()[drawModeIndex];


        img = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
//*
        rasterImage = new RasterImageAWT<>(
                img,
                //Function<PixelType,Integer>, kde PixelType = Color
                (Color c) -> c.getRGB(),
                //Function<Integer,PixelType>, kde PixelType = Color
                (Integer i) -> new Color(i)
        );

        presenter = new ImagePresenterAWT<>();
/*/
        rasterImage = RasterImageImmu.cleared(
				width, height, new Color(255,0,0));
		presenter = new ImagePresenterUniversal<>(color -> color.getRGB());
//*/
        lineRasterizer = new LineRasterizerDDA<>();
        circleRasterizer = new CircleRasterizerAWT<>();


        panelMenu = new JPanel();
        panelMenu.setBackground(new Color(0x777777));
        panelMenu.setPreferredSize(new Dimension(300, 600));
        panelMenu.setLayout(new BoxLayout(panelMenu, BoxLayout.Y_AXIS));

        JPanel panelColors = new JPanel(), panelColor = new JPanel();
        JLabel colorlbl = new JLabel("Color");
        modelbl = new JLabel("Mode: " + dm.name());
        JSlider colorR = new JSlider(JSlider.HORIZONTAL, 0, 255, 10);
        JSlider colorG = new JSlider(JSlider.HORIZONTAL, 0, 255, 127);
        JSlider colorB = new JSlider(JSlider.HORIZONTAL, 0, 255, 255);

        panelColors.setLayout(new BoxLayout(panelColors, BoxLayout.Y_AXIS));

        panelColor.setPreferredSize(new Dimension(250, 100));
        colorR.addChangeListener(e -> panelColor.setBackground(new Color(colorR.getValue(), colorG.getValue(), colorB.getValue())));
        colorG.addChangeListener(e -> panelColor.setBackground(new Color(colorR.getValue(), colorG.getValue(), colorB.getValue())));
        colorB.addChangeListener(e -> panelColor.setBackground(new Color(colorR.getValue(), colorG.getValue(), colorB.getValue())));

        panelColors.add(colorlbl);
        panelColors.add(colorR);
        panelColors.add(colorG);
        panelColors.add(colorB);
        panelColors.add(panelColor);
        panelMenu.add(modelbl);
        panelMenu.add(panelColors);

        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (firstStep || dm != DrawMode.CircleSector) {
                        startC = e.getX();
                        startR = e.getY();
                    } else
                        arrRast.remove(arrRast.size() - 1);
                }
                if (SwingUtilities.isRightMouseButton(e)) {
                    changeDrawMode();
                    if (dm == DrawMode.Polygon) {
                        polygonMemory = new PolygonData<>(lineRasterizer, new ArrayList<>(), new Color(colorR.getValue(), colorG.getValue(), colorB.getValue()));
                    }
                    if (isntPolygonSaved && polygonMemory.getSize() > 0) {
                        isntPolygonSaved = !isntPolygonSaved;
                        arrRast.add(new PolygonData<>(polygonMemory.getLineRasterizer(), polygonMemory.getList(), polygonMemory.getColor()));
                    }
                    firstStep = true;
                }
            }

            public void mouseReleased(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    final int endC = e.getX();
                    final int endR = e.getY();

                    //přepočet od 0 do 1
                    final double x1 =
                            2 * (double) startC / rasterImage.getWidth() - 1;
                    final double y1 =
                            -(2 * (double) startR / rasterImage.getHeight() - 1);
                    final double x2 =
                            2 * (double) endC / rasterImage.getWidth() - 1;
                    final double y2 =
                            -(2 * (double) endR / rasterImage.getHeight() - 1);

                    clearWithAxes();
                    switch (dm) {
                        case Line:
                            arrRast.add(new LineData<>(lineRasterizer, x1, y1, x2, y2, new Color(colorR.getValue(), colorG.getValue(), colorB.getValue())));
                            break;

                        case Polygon:

                            if (firstStep) {
                                firstStep = false;
                                polygonMemory.addPoint(new Point2D(x1, y1));
                            }
                            polygonMemory.addPoint(new Point2D(x2, y2));
                            polygonMemory.setColor(new Color(colorR.getValue(), colorG.getValue(), colorB.getValue()));
                            polygonMemory.setLineRasterizer(lineRasterizer);
                            isntPolygonSaved = true;

                            break;

                        case Circle:
                        case CircleSector:
                            double s1 = e.getX() - startC, s2 = e.getY() - startR;
                            if (dm == DrawMode.Circle || (dm == DrawMode.CircleSector && firstStep)) {
                                radius = Math.sqrt(Math.pow(s1, 2) + Math.pow(s2, 2));
                                angle1 = Math.toDegrees(Math.atan2(s2, s1));
                                arrRast.add(new CircleData<>(circleRasterizer, lineRasterizer, x1, y1, radius, 0, 0, new Color(colorR.getValue(), colorG.getValue(), colorB.getValue())));
                                if (dm == DrawMode.CircleSector) {
                                    firstStep = !firstStep;
                                }
                            } else if (dm == DrawMode.CircleSector && !firstStep) {
                                firstStep = !firstStep;
                                arrRast.add(new CircleData<>(circleRasterizer, lineRasterizer, startC, startR, radius, angle1, angle2, new Color(colorR.getValue(), colorG.getValue(), colorB.getValue())));
                            }
                            break;

                        default:
                            rasterImage.withPixel(endC, endR, new Color(colorR.getValue(), colorG.getValue(), colorB.getValue()));
                            break;
                    }
                    redraw();
                    panel.repaint();

                }

            }
        });
        panel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {

                if (SwingUtilities.isLeftMouseButton(e)) {
                    Color hl = new Color(0x1fff8f);

                    final int endC = e.getX();
                    final int endR = e.getY();


                    //přepočet od 0 do 1
                    final double x1 =
                            2 * (double) startC / rasterImage.getWidth() - 1;
                    final double y1 =
                            -(2 * (double) startR / rasterImage.getHeight() - 1);
                    final double x2 =
                            2 * (double) endC / rasterImage.getWidth() - 1;
                    final double y2 =
                            -(2 * (double) endR / rasterImage.getHeight() - 1);


                    clearWithAxes();
                    redraw();

                    switch (dm) {
                        case Line:
                            rasterImage = lineRasterizer.rasterize(rasterImage, x1, y1, x2, y2, hl);
                            break;

                        case Polygon:
                            if (firstStep) {
                                polygonMemory.addPoint(new Point2D(x1, y1));
                            }
                            polygonMemory.addPoint(new Point2D(x2,y2));
                            polygonMemory.drawElement(rasterImage);
                            polygonMemory.removeLastPoint();
                            break;

                        case Circle:
                            double s1 = e.getX() - startC, s2 = e.getY() - startR;
                            radius = Math.sqrt(Math.pow(s1, 2) + Math.pow(s2, 2));
                            angle1 = Math.toDegrees(Math.atan2(s2, s1));
                            rasterImage = lineRasterizer.rasterize(rasterImage, x1, y1, x2, y2, hl);
                            rasterImage = circleRasterizer.drawCircle(rasterImage, x1, y1, radius, hl);
                            break;

                        case CircleSector:
                            if (firstStep) {
                                s1 = e.getX() - startC;
                                s2 = e.getY() - startR;
                                radius = Math.sqrt(Math.pow(s1, 2) + Math.pow(s2, 2));
                                angle1 = Math.toDegrees(Math.atan2(s2, s1));
                                rasterImage = lineRasterizer.rasterize(rasterImage, x1, y1, x2, y2, hl);
                                rasterImage = circleRasterizer.drawCircle(rasterImage, x1, y1, radius, hl);
                                break;
                            } else {
                                s1 = e.getX() - startC;
                                s2 = e.getY() - startR;
                                angle2 = Math.toDegrees(Math.atan2(s2, s1));
                                rasterImage = lineRasterizer.rasterize(rasterImage, x1, y1, x2, y2, hl);
                                rasterImage = circleRasterizer.drawCircleSector(rasterImage, lineRasterizer, startC, startR, radius, angle1, angle2, hl);
                                break;
                            }

                        default:
                            rasterImage.withPixel(endC, endR, new Color(colorR.getValue(), colorG.getValue(), colorB.getValue()));
                            break;
                    }
                    System.out.println("XXXXXXX");

                }
                panel.repaint();
            }

        });

        frame.add(panel, BorderLayout.WEST);
        frame.add(panelMenu, BorderLayout.EAST);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Canvas(800, 600)::start);
    }

    private void changeDrawMode() {
        if (drawModeIndex < DrawMode.values().length - 1)
            drawModeIndex++;
        else
            drawModeIndex = 0;

        dm = DrawMode.values()[drawModeIndex];
        modelbl.setText("Mode: " + dm.name());
    }

    private void redraw() {
        clearWithAxes();
        for (ObjectData o : arrRast) {
            rasterImage = o.drawElement(rasterImage);
        }
        if (polygonMemory != null) {
            polygonMemory.drawElement(rasterImage);
        }
    }

    private void clearWithAxes() {
        clear();
        for (int i = 0; i < rasterImage.getWidth(); i++) {
            rasterImage = rasterImage.withPixel(i, rasterImage.getHeight() / 2,
                    new Color(0x333333));
        }
        for (int j = 0; j < rasterImage.getHeight(); j++) {
            rasterImage = rasterImage.withPixel(rasterImage.getWidth() / 2, j,
                    new Color(0x333333));
        }
    }

    public void clear() {
        //rasterImage = rasterImage.cleared(new Color(0x4444444));
        rasterImage = new RasterImageAWT<Color>(img,
                //Function<PixelType,Integer>, kde PixelType = Color
                (Color c) -> c.getRGB(),
                //Function<Integer,PixelType>, kde PixelType = Color
                (Integer i) -> new Color(i)).cleared(new Color(0x4444444));

    }

    public void present(final Graphics graphics) {
        presenter.present(rasterImage, graphics);
    }

    public void draw() {
        clearWithAxes();
//		img.setRGB(10, 10, 16776960);
//		img.setRGB(10, 10, 0xffff00);

        rasterImage = circleRasterizer.drawCircleSector(rasterImage, lineRasterizer, 400, 300, 50, 20, 365, new Color(0x1fff8f));

        rasterImage = circleRasterizer.drawCircle(rasterImage, 0, 0, 80, new Color(0xFF0C07));




		/*rasterImage = lineRasterizer.rasterize(
                rasterImage,
				-1, -1, 1, 1,
				new Color(1.0f, 0, 1)
		);

		rasterImage = rasterImage.withPixel(10,10,
                new Color(0xffff00));*/

		/*rasterImage = new SeedFill4<Color>().fill(rasterImage,
                rasterImage.getWidth() / 2, rasterImage.getHeight() / 2 + 5,
				new Color(0x00ffff),
				pixel -> pixel.equals(new Color(0x2f, 0x2f, 0x2f)));*/

    }

    public void start() {
        draw();
        panel.repaint();
    }

    private enum DrawMode {Line, Circle, CircleSector, Polygon}

}