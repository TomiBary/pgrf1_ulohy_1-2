package rasterizationops;


import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;
import java.util.Optional;
import java.util.function.Predicate;

public class SeedFill4<PixelType> implements SeedFill<PixelType> {

    @NotNull
    @Override
    public RasterImage<PixelType> fill(
            @NotNull RasterImage<PixelType> image,
            int c, int r,
            @NotNull PixelType value,
            @NotNull Predicate<PixelType> validPixel) {
        return image.getPixel(c, r).flatMap(
            pixel -> {
                if (validPixel.test(pixel)) {
                    return Optional.of(
                        fill(
                            fill(
                                fill(
                                    fill(
                                            image.withPixel(c, r, value),
                                    c + 1, r, value, validPixel),
                                c, r + 1, value, validPixel),
                            c - 1, r, value, validPixel),
                        c, r - 1, value, validPixel)
                    );
                }
                return Optional.empty();
            }
        ).orElse(image);
    }
}
