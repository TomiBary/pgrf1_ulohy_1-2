package rasterizationops;

import io.vavr.collection.Stream;
import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

public class LineRasterizerDDA<PixelType> implements LineRasterizer<PixelType> {

    @NotNull
    @Override
    public  RasterImage<PixelType> rasterize(
        @NotNull RasterImage<PixelType> background,
        double x1, double y1, double x2, double y2,
        @NotNull PixelType value) {

        double ix1 = (x1 + 1) * background.getWidth() / 2;
        double iy1 = (-y1 + 1) * background.getHeight() / 2;
        double ix2 = (x2 + 1) * background.getWidth() / 2;
        double iy2 = (-y2 + 1) * background.getHeight() / 2;
        return rasterizeRaster(background, ix1, iy1, ix2, iy2, value);
    }

    public  RasterImage<PixelType> rasterizeRaster(
            @NotNull RasterImage<PixelType> background,
    double ix1, double iy1, double ix2, double iy2,
    @NotNull PixelType value) {

        final double dx = ix2-ix1;
        final double dy = iy2-iy1;

        if (Math.abs(iy2 - iy1) <= Math.abs(ix2 - ix1)) {

            if ((ix1 == ix2) && (iy1 == iy2)) {
                return background.withPixel((int)ix1, (int)iy1, value);

            } else {
                if (ix2 < ix1) {
                    double tmp = ix2;
                    ix2 = ix1;
                    ix1 = tmp;

                    tmp = iy2;
                    //iy2 = iy1;
                    iy1 = tmp;
                }

                double k = dy/dx;
                final double y = iy1;

                // Try with Stream
                final int fx1, fx2;
                fx1 = (int)ix1;
                fx2 = (int)ix2;
                return Stream.rangeClosed(fx1, fx2).foldLeft(background,
                        (currentImage, c) -> {
                            final int r = (int) (y + (c - fx1) * k);
                            return currentImage.withPixel(c, r, value);
                        }
                );
            }
        } else {

            if (iy2 < iy1) {
                double tmp = ix2;
                //ix2 = ix1;
                ix1 = tmp;

                tmp = iy2;
                iy2 = iy1;
                iy1 = tmp;
            }

            // Try with For loop
            double k = dx/dy;
            int c;
            double x = ix1;
            for (int r = (int)iy1; r <= (int)iy2; r++) {
                c = (int)Math.round(x);
                background.withPixel(c, r, value);
                x += k;
            }
            return background;
        }
    }
}
