package rasterizationops;

import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

import java.util.function.Predicate;

public interface SeedFill<PixelType> {
    @NotNull RasterImage<PixelType> fill(
            @NotNull RasterImage<PixelType> image,
            int c, int r,
            @NotNull PixelType value,
            @NotNull Predicate<PixelType> validPixel
    );
}
