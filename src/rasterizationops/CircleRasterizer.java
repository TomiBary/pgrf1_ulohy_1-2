package rasterizationops;

import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

public interface CircleRasterizer<T> {
        /**
         * Rasterizes a circle in normalized coordinates ([-1;1] square),
         * upper image left corner in [-1;1], lower left corner in [1;-1]
         * @param background image to "add" the line to
         * @param x1 x-coordinate of the start-point, in [-1;1]
         * @param y1 y-coordinate of the start-point, in [-1;1]
            ???
         * @param value value of the line pixels
         * @return new image with the circle added on the background
         */
        @NotNull RasterImage<T> drawCircleSector(
            @NotNull RasterImage<T> background,
            @NotNull LineRasterizer<T> lineRasterizer,
            double x1, double y1, double radius, double angle1, double angle2,
            @NotNull T value);

        @NotNull RasterImage<T> drawCircle(
            @NotNull RasterImage<T> background,
            double x1, double y1, double radius, @NotNull T value);
}