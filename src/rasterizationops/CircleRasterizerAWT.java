package rasterizationops;

import org.jetbrains.annotations.NotNull;
import rasterdata.PolygonData;
import rasterdata.RasterImage;
import transforms.Point2D;


public class CircleRasterizerAWT<PixelType> implements CircleRasterizer<PixelType>{

    private PolygonData polygon = new PolygonData();

    @NotNull
    @Override
    public RasterImage<PixelType> drawCircle(
            @NotNull RasterImage<PixelType> background,
            double x1, double y1, double radius, @NotNull PixelType value)
    {
        //zpět do rozmezí 800x600
        double ix1 = (x1 + 1) * background.getWidth() / 2;
        double iy1 = (-y1 + 1) * background.getHeight() / 2;

        int x = 0, y = (int)radius;
        int d = 3 - 2 * (int)radius;
        while (y >= x)
        {
            drawCirclePoints(background, (int)ix1, (int)iy1, x, y, value);
            x++;

            if (d > 0)
            {
                y--;
                d = d + 4 * (x - y) + 10;
            }
            else
                d = d + 4 * x + 6;

            drawCirclePoints(background, (int)ix1, (int)iy1, x, y, value);
        }

        return background;
    }

    private @NotNull RasterImage<PixelType> drawCirclePoints(@NotNull RasterImage<PixelType> bg, int xc, int yc, int x, int y, @NotNull PixelType value)
    {
        bg.withPixel(xc+x, yc+y, value);
        bg.withPixel(xc-x, yc+y, value);
        bg.withPixel(xc+x, yc-y, value);
        bg.withPixel(xc-x, yc-y, value);
        bg.withPixel(xc+y, yc+x, value);
        bg.withPixel(xc-y, yc+x, value);
        bg.withPixel(xc+y, yc-x, value);
        bg.withPixel(xc-y, yc-x, value);

        return bg;
    }

    //pocitani a kresleni kruznice
    public @NotNull RasterImage<PixelType> drawCircleSector(
            @NotNull RasterImage<PixelType> background,
            @NotNull LineRasterizer<PixelType> lineRasterizer,
            double x1, double y1, double radius, double angle1, double angle2,
            @NotNull PixelType value)
    //public void drawCircleSector(int x1, int y1, int x2, int y2)
    {
        polygon.clearList();

        if (angle1 < 0){
            angle1 = 360+angle1;
        }

        if (angle2 < 0){
            angle2 = 360+angle2;
        }
        if (angle1 > angle2){
            angle2 = 360 + angle2;
        }

        double a1 = Math.toRadians(angle1);
        double a2 = Math.toRadians(angle2);
        if (a1 > a2){
            double pom = a1;
            a1 = a2;
            a2 = pom;
        }

        for (double fi = a1; fi <= a2; fi += 0.001) {
            double x = radius * Math.cos(fi);
            double y = radius * Math.sin(fi);

            Point2D point = new Point2D( x+x1, y+y1);
            polygon.addPoint(point);

        }

        for(int i=0; i < polygon.getSize()-1; i++) {
            Point2D from = polygon.getPoint(i);
            Point2D to = polygon.getPoint(i+1);
            background = lineRasterizer.rasterizeRaster(background, from.getX(), from.getY(), to.getX(), to.getY(), value);
        }

        return background;

    }
}