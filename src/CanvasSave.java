import org.jetbrains.annotations.NotNull;
import rasterdata.*;
import rasterizationops.CircleRasterizer;
import rasterizationops.CircleRasterizerAWT;
import rasterizationops.LineRasterizer;
import rasterizationops.LineRasterizerDDA;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

/* terminy uloh
uloha1: 26.12.2017
uloha2: 31.12.2017
uloha3: 7.1.2018
 */

public class CanvasSave {

	private final JFrame frame;
	private final JPanel panel, panelMenu;
	private final BufferedImage img;

	private final List<Point> points, nPoints;
	private enum DrawMode{Point, Line, Circle, CircleSector, Polygon}

	private @NotNull RasterImage<Color> rasterImage;
	private final @NotNull ImagePresenter<Color, Graphics> presenter;

	private final @NotNull LineRasterizer<Color> lineRasterizer;
	private final @NotNull CircleRasterizer<Color> circleRasterizer;
	private int startC, startR;

	public CanvasSave(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


		//List pro úsečky
		points = new ArrayList<>();
        //List pro nuhelník
		nPoints = new ArrayList<>();
		//What to draw mode


		img = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
//*
		rasterImage = new RasterImageAWT<>(
			img,
			//Function<PixelType,Integer>, kde PixelType = Color
				(Color c) -> c.getRGB(),
			//Function<Integer,PixelType>, kde PixelType = Color
				(Integer i) -> new Color(i)
		);

		presenter = new ImagePresenterAWT<>();
/*/
		rasterImage = RasterImageImmu.cleared(
				width, height, new Color(255,0,0));
		presenter = new ImagePresenterUniversal<>(color -> color.getRGB());
//*/
		lineRasterizer = new LineRasterizerDDA<>();
		circleRasterizer = new CircleRasterizerAWT<>();


		panelMenu = new JPanel();
		panelMenu.setBackground(new Color(1f,0,1f));
		panelMenu.setPreferredSize(new Dimension(300, 600));

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				startC = e.getX();
				startR = e.getY();

			}

            @Override
            public void mouseReleased(MouseEvent e) {
                if(SwingUtilities.isLeftMouseButton(e)){
                    final int endC = e.getX();
                    final int endR = e.getY();

                    //přepočet od 0 do 1
                    final double x1 =
                            2 * (double) startC / rasterImage.getWidth() - 1;
                    final double y1 =
                            -(2 * (double) startR / rasterImage.getHeight() - 1);
                    final double x2 =
                            2 * (double) endC / rasterImage.getWidth() - 1;
                    final double y2 =
                            -(2 * (double) endR / rasterImage.getHeight() - 1);

                    /*Point point;
                    if (0 < nPoints.size()) {
                        nPoints.add(
                                new Point(
                                        nPoints.get(nPoints.size()-1).getX2(), nPoints.get(nPoints.size()-1).getY2(), x2, y2
                                )
                        );
                    }else{
                        point = new Point(x1, y1, x2, y2);
                        nPoints.add(point);
                    }


                    System.out.println("POINT ADDED");*/
                }

            }
        });
		panel.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {

                if(SwingUtilities.isLeftMouseButton(e)) {
                    final int endC = e.getX();
                    final int endR = e.getY();
                    final double x1 =
                            2 * (double) startC / rasterImage.getWidth() - 1;
                    final double y1 =
                            -(2 * (double) startR / rasterImage.getHeight() - 1);
                    final double x2 =
                            2 * (double) endC / rasterImage.getWidth() - 1;
                    final double y2 =
                            -(2 * (double) endR / rasterImage.getHeight() - 1);

                    System.out.println("X1: " + x1 + " Y1: " + y1 + " X2: " + x2 + " Y2: " + y2);

                    clearAndRedraw();

                    //draw new lines
                    /*if (0 < nPoints.size()) {
                        rasterImage = lineRasterizer.rasterize(
                                rasterImage,
                                nPoints.get(0).getX1(), nPoints.get(0).getY1(), x2, y2,
                                new Color(0x8080ff)
                        );

                        rasterImage = lineRasterizer.rasterize(
                                rasterImage,
                                nPoints.get(nPoints.size() - 1).getX2(), nPoints.get(nPoints.size() - 1).getY2(), x2, y2,
                                new Color(0x8080ff)
                        );

                    } else {
                        rasterImage = lineRasterizer.rasterize(
                                rasterImage,
                                x1, y1, x2, y2,
                                new Color(0x8080ff)
                        );
                    }*/
                }
                if(SwingUtilities.isRightMouseButton(e)) {
                	clear();
                    final double x1 =
                            2 * (double) startC / rasterImage.getWidth() - 1;// -0.3 pro 300    když 800x600
                    final double y1 =
                            -(2 * (double) startR / rasterImage.getHeight() - 1); //0.33333 pro 200
                    /*final double x2 =
                            2 * (double) e.getX() / rasterImage.getWidth() - 1;
                    final double y2 =
                            -(2 * (double) e.getY() / rasterImage.getHeight() - 1);*/


					double s1 = e.getX() - startC, s2 = e.getY() - startR;
                    double radius = Math.sqrt(Math.pow(s1, 2) + Math.pow(s2, 2));
					double angle1 = Math.toDegrees(Math.atan2(s2, s1));

                    System.out.println("StartC a R: " + startC + " " + startR + " X1 a Y1: " + x1 + " " + y1);


					//calculate end Angle of circle
					System.out.println("MathAcos: " + angle1);
					double angle2 = 0;

					rasterImage = circleRasterizer.drawCircle(rasterImage, x1, y1, radius, new Color(0x000EFF));
					rasterImage = circleRasterizer.drawCircleSector(rasterImage, lineRasterizer, startC, startR, radius, angle1, angle2, new Color(0x1fff8f));

                }
				panel.repaint();
			}

        });

		frame.add(panel, BorderLayout.WEST);
		frame.add(panelMenu, BorderLayout.EAST);
		frame.pack();
		frame.setVisible(true);
	}

	private void clearAndRedraw() {

		clear();

		//Load saved lines
		/*if (0 < nPoints.size()) {
			for (Point point : nPoints) {
				rasterImage = lineRasterizer.rasterize(
						rasterImage,
						point.getX1(), point.getY1(), point.getX2(), point.getY2(),
						new Color(0x8080ff)
				);
			}
		}*/


	}

	private void clearWithAxes(){
		clear();
	    for (int i = 0; i < rasterImage.getWidth(); i++){
            rasterImage = rasterImage.withPixel(i,rasterImage.getHeight() / 2,
                    new Color(0x414141));
        }
        for (int j = 0; j < rasterImage.getHeight(); j++){
            rasterImage = rasterImage.withPixel(rasterImage.getWidth() / 2, j,
                    new Color(0x414141));
        }
    }

	public void clear() {
		rasterImage = rasterImage.cleared(new Color(0x2f, 0x2f, 0x2f));

	}

	public void present(final Graphics graphics) {
		presenter.present(rasterImage, graphics);
	}

	public void draw() {
		clearWithAxes();
//		img.setRGB(10, 10, 16776960);
//		img.setRGB(10, 10, 0xffff00);

        rasterImage = circleRasterizer.drawCircleSector(rasterImage, lineRasterizer, 400, 300, 50,0,300, new Color(0x1fff8f));

		rasterImage = circleRasterizer.drawCircle(rasterImage, 0, 0, 80, new Color(0xFF0C07));


		/*rasterImage = lineRasterizer.rasterize(
				rasterImage,
				-1, -1, 1, 1,
				new Color(1.0f, 0, 1)
		);

		rasterImage = rasterImage.withPixel(10,10,
                new Color(0xffff00));*/

		/*rasterImage = new SeedFill4<Color>().fill(rasterImage,
				rasterImage.getWidth() / 2, rasterImage.getHeight() / 2 + 5,
				new Color(0x00ffff),
				pixel -> pixel.equals(new Color(0x2f, 0x2f, 0x2f)));*/

	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new CanvasSave(800, 600)::start);
	}

}